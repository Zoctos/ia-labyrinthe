#ifndef LABYRINTH_H
#define LABYRINTH_H

#include <string>
#include <iostream>
#include <fstream>
#include <SFML\Graphics.hpp>

#include "Cell.hpp"

class Labyrinth {

private:
	int** level;
	int width;
	int height;
	int posBegin[2];
	Cell *sortie;

	void loadLevelWithFilename(std::string filename);

public:

	Labyrinth(std::string filename);

	int getCase(int x, int y);
	Cell* getCell(int x, int y);
	bool isCell(int x, int y);

	Cell* getSortie();
	int* getPosBegin();

	void render(sf::RenderWindow *window);

	std::vector<Cell*> *getSuccesseurs(Cell* cell);

};
#endif