#ifndef ENTITYIA_H
#define ENTITYIA_H

#include "Labyrinth.hpp"
#include "Node.hpp"

class EntityIA {

protected :

	int x;
	int y;
	Labyrinth* labyrinth;

public :
	EntityIA(Labyrinth* labyrinth);
	~EntityIA();

	int getX();
	int getY();

	virtual Node* executeAlgo();

	void render(sf::RenderWindow *window);

};
#endif