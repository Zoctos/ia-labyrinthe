#ifndef NODE_HPP
#define NODE_HPP

#include <vector>
#include <iostream>

#include "SFML\Graphics.hpp"
#include "Cell.hpp"

class Node {

private:
	Cell *content;
	int value;
	Node *parent;
	std::vector<Node*> *fils;
	bool isChemin;

public:
	Node(Cell *content, int value);
	
	void setParent(Node* parent);
	void addFils(Node* fils);
	
	std::vector<Node*> *getFils();
	Node *getParent();
	Node *getFirstParent();
	Cell *getContent();
	int getValue();
	int getProfondeurMax();
	bool isCheminSortie();
	
	int getLongueurChemin();
	int getNombreNoeuds();

	bool isSame(Node *n);

	void render(sf::RenderWindow *window, int avancementChemin);
	void renderFils(sf::RenderWindow *window, int profondeur);
	void renderVersSortie(sf::RenderWindow *window, int profondeur);

};
#endif