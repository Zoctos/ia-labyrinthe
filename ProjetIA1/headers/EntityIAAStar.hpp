#ifndef ENTITYIA_ASTAR_HPP
#define ENTITYIA_ASTAR_HPP

#include "EntityIA.hpp"

class EntityIAAStar : public EntityIA {

public:
	EntityIAAStar(Labyrinth *labyrinth);

	virtual Node *executeAlgo();

};

#endif