#ifndef ENTITYIA_PLAYER_HPP
#define ENTITYIA_PLAYER_HPP

#include "EntityIA.hpp"

class EntityPlayer : public EntityIA {

public:
	EntityPlayer(Labyrinth *labyrinth);

	void setX(int x);
	void setY(int y);

	void render(sf::RenderWindow *window);

};

#endif