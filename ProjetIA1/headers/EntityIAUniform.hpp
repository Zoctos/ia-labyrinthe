#ifndef ENTITYIA_UNIFORM_HPP
#define ENTITYIA_UNIFORM_HPP

#include <queue>
#include <set>
#include <vector>
#include <stack>


#include "EntityIA.hpp"
#include "Labyrinth.hpp"
#include "Node.hpp"

class EntityIAUniform : public EntityIA{

public :
	EntityIAUniform(Labyrinth *labyrinth);

	virtual Node* executeAlgo();

};

#endif