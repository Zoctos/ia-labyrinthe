#ifndef ENTITYIA_BEST_FIRST_H
#define ENTITYIA_BEST_FIRST_H

#include <queue>
#include <set>
#include <vector>
#include <stack>

#include "EntityIA.hpp"
#include "Cell.hpp"
#include "Node.hpp"

class EntityIABestFirst : public EntityIA {

public :
	EntityIABestFirst(Labyrinth *labyrinth);

	virtual Node* executeAlgo();

};
#endif