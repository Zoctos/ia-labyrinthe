#ifndef CELL_H
#define CELL_H

#include <vector>

class Cell {

private:
	int x;
	int y;
	int type;

public:

	const static int CELL_TYPE_EXIT = -1;
	const static int CELL_TYPE_WAY = 0;
	const static int CELL_TYPE_WALL = 1;

	Cell(int x, int y, int type);

	int getX();
	int getY();
	int getType();

	int getDistance(Cell *c);

};
#endif