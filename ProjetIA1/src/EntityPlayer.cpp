#include "../headers/EntityPlayer.h"

EntityPlayer::EntityPlayer(Labyrinth *labyrinth) : EntityIA(labyrinth) {

}

void EntityPlayer::setX(int x) {
	if(labyrinth->isCell(x, this->y) && labyrinth->getCell(x, this->y)->getType() != Cell::CELL_TYPE_WALL)
		this->x = x;
}

void EntityPlayer::setY(int y) {
	if (labyrinth->isCell(this->x, y) && labyrinth->getCell(this->x, y)->getType() != Cell::CELL_TYPE_WALL)
		this->y = y;
}

void EntityPlayer::render(sf::RenderWindow *window) {

	sf::RectangleShape rect(sf::Vector2f(15.0f, 15.0f));
	rect.setPosition(sf::Vector2f(this->x*20+2.5, this->y*20+2.5));
	rect.setFillColor(sf::Color::White);

	window->draw(rect);

}