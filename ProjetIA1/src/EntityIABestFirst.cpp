#include "../headers/EntityIABestFirst.hpp"

EntityIABestFirst::EntityIABestFirst(Labyrinth *labyrinth) : EntityIA(labyrinth) {

}


bool comparatorNode(Node *n1, Node *n2) {

	int d1 = n1->getValue();
	int d2 = n2->getValue();

	return (d1 < d2);
}

Node* EntityIABestFirst::executeAlgo() {

	std::stack<Node*> *liste = new std::stack<Node*>();
	std::vector<Node*> parcouru;

	Node *exit = new Node(labyrinth->getSortie(), 0);

	Node *begin = new Node(labyrinth->getCell(x, y), labyrinth->getCell(x, y)->getDistance(exit->getContent()));
	Node *end = NULL;

	liste->push(begin);

	while (liste->size() != 0) {
		
		Node *currentNode = liste->top();
		liste->pop();
		Cell *courant = currentNode->getContent();
		parcouru.push_back(currentNode);

		if (courant->getType() == Cell::CELL_TYPE_EXIT) {
			end = currentNode;
			break;
		}
		else {

			std::vector<Cell*> *success = labyrinth->getSuccesseurs(courant);
			std::vector<Node*> *tmp = new std::vector<Node*>();

			for (int i = 0; i < success->size(); i++) {

				Node *successor = new Node(success->at(i), success->at(i)->getDistance(exit->getContent()));
				successor->setParent(currentNode);

				int indexList = -1;
				for (int e = 0; e < parcouru.size(); e++) {
					if (parcouru[e]->isSame(successor))
						indexList = e;
				}

				if(indexList != -1){

					Node *prevNode = parcouru[indexList];
					if(prevNode->getValue() <= successor->getValue())
						continue;	
					else {
						parcouru.erase(parcouru.begin() + indexList);
						parcouru.push_back(successor);
					}
				}
				else {
				
					tmp->push_back(successor);

				}

			}

			std::sort(tmp->begin(), tmp->end(), comparatorNode);
			
			for (int i = tmp->size()-1; i >= 0; i--) {
				liste->push(tmp->at(i));
				currentNode->addFils(tmp->at(i));
			}
			

		}

	}

	return end;

}
