#include "../headers/EntityIAUniform.hpp"

EntityIAUniform::EntityIAUniform(Labyrinth *labyrinth) : EntityIA(labyrinth) {

}

bool comparatorNodeUniform(Node *n1, Node *n2) {
	int d1 = n1->getValue();
	int d2 = n2->getValue();

	return (d1 < d2);
}

Node *EntityIAUniform::executeAlgo() {

	std::vector<Node*> *liste = new std::vector<Node*>();
	std::vector<Node*> parcouru;

	Node *exit = new Node(labyrinth->getSortie(), 0);

	Node *begin = new Node(labyrinth->getCell(x, y), 0);
	Node *end = NULL;

	liste->push_back(begin);

	while (liste->size() != 0) {

		Node *currentNode = liste->at(0);
		liste->erase(liste->begin());
		Cell *courant = currentNode->getContent();
		parcouru.push_back(currentNode);

		if (courant->getType() == Cell::CELL_TYPE_EXIT) {
			end = currentNode;
			break;
		}
		else {

			std::vector<Cell*> *success = labyrinth->getSuccesseurs(courant);
			std::vector<Node*> *tmp = new std::vector<Node*>();

			for (int i = 0; i < success->size(); i++) {

				Node *successor = new Node(success->at(i), currentNode->getValue()+1);
				successor->setParent(currentNode);

				int indexList = -1;
				for (int e = 0; e < parcouru.size(); e++) {
					if (parcouru[e]->isSame(successor))
						indexList = e;
				}

				if (indexList != -1) {

					Node *prevNode = parcouru[indexList];
					if (prevNode->getValue() <= successor->getValue())
						continue;
					else {
						parcouru.erase(parcouru.begin() + indexList);
						parcouru.push_back(successor);
					}
				}
				else {

					tmp->push_back(successor);

				}

			}

			for (int i = tmp->size() - 1; i >= 0; i--) {
				liste->push_back(tmp->at(i));
				currentNode->addFils(tmp->at(i));
			}

			std::sort(liste->begin(), liste->end(), comparatorNodeUniform);

		}

	}

	return end;

}