#include "../headers/Cell.hpp"

Cell::Cell(int x, int y, int type) {
	this->x = x;
	this->y = y;
	this->type = type;
}

int Cell::getX() {
	return x;
}

int Cell::getY() {
	return y;
}

int Cell::getType() {
	return type;
}

int Cell::getDistance(Cell *c) {

	return abs(getX() - c->getX()) + abs(getY() - c->getY());

}