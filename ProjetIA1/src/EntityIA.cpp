#include "../headers/EntityIA.hpp"

EntityIA::EntityIA(Labyrinth *labyrinth) {

	this->labyrinth = labyrinth;
	this->x = labyrinth->getPosBegin()[0];
	this->y = labyrinth->getPosBegin()[1];

}

int EntityIA::getX() {
	return x;
}

int EntityIA::getY() {
	return y;
}

void EntityIA::render(sf::RenderWindow *window) {

	sf::CircleShape circle(10.0f);
	circle.setPosition(sf::Vector2f(x * 20.0, y * 20.0));
	circle.setFillColor(sf::Color::Yellow);

	window->draw(circle);

}

Node* EntityIA::executeAlgo() {
	return NULL;
}