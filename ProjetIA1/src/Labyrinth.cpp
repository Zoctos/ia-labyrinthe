#include "../headers/Labyrinth.hpp"

Labyrinth::Labyrinth(std::string filename) {

	loadLevelWithFilename(filename);

}

void Labyrinth::loadLevelWithFilename(std::string filename) {

	std::ifstream file(filename, std::ios::in);

	if (file) {

		std::string line;

		//recuperer la position de l'entite au d�but
		getline(file, line);
		this->posBegin[0] = stoi(line);
		getline(file, line);
		this->posBegin[1] = stoi(line);

		//recuperer la taille du labyrinthe
		getline(file, line);
		this->width = stoi(line);
		getline(file, line);
		this->height = stoi(line);

		//initialisation du tableau du niveau
		level = new int*[height];
		int yPos = 0;

		//pour chaque ligne du niveau
		while (getline(file, line)) {

			level[yPos] = new int[width];

			//on recupere chaque case
			for (int xPos = 0; xPos < line.size(); xPos++) {

				char current = line.at(xPos);
				int n;

				//si dans le fichier on a un - c'est la sortie
				if (current == '-') {
					current = line.at(xPos+1);
					n = current - '0';
					n = -n;
					level[yPos][xPos] = n;
					xPos++;
					sortie = new Cell(xPos, yPos, n);
				}
				else {
					n = current - '0';
					level[yPos][xPos] = n;
				}

			}
			yPos++;
		}

		file.close();

	}
	else {
		std::cerr << "Error " << std::endl;
	}

}

int Labyrinth::getCase(int x, int y) {
	return level[y][x];
}

Cell *Labyrinth::getCell(int x, int y) {
	if (x < 0 || y < 0 || x >= width || y >= height)
		return NULL;
	return new Cell(x, y, level[y][x]);
}

bool Labyrinth::isCell(int x, int y) {
	if (x < 0 || y < 0 || x >= width || y >= height)
		return false;
	return true;
}

Cell *Labyrinth::getSortie() {
	return sortie;
}

int* Labyrinth::getPosBegin() {
	return posBegin;
}

void Labyrinth::render(sf::RenderWindow *window) {

	for (int y = 0; y < height; y++) {

		for (int x = 0; x < width; x++) {

			sf::RectangleShape rect(sf::Vector2f(20.0, 20.0));
			rect.setPosition(sf::Vector2f(x * 20, y * 20));

			switch (getCase(x, y)) {

				case Cell::CELL_TYPE_EXIT :
						rect.setFillColor(sf::Color::Blue);
					break;

				case Cell::CELL_TYPE_WALL :
						rect.setFillColor(sf::Color::Red);
					break;

				case Cell::CELL_TYPE_WAY :
						rect.setFillColor(sf::Color::Green);
					break;

			}

			window->draw(rect);

		}

	}

}

std::vector<Cell*> *Labyrinth::getSuccesseurs(Cell *cell) {

	int x = cell->getX();
	int y = cell->getY();

	std::vector<Cell*> *success = new std::vector<Cell*>();
	
	if(x+1 < this->width)
		success->push_back(getCell(x + 1, y));

	if(x-1 >= 0)
		success->push_back(getCell(x - 1, y));

	if(y+1 < this->height)
		success->push_back(getCell(x, y + 1));

	if(y - 1 >= 0)
		success->push_back(getCell(x, y - 1));

	for (int i = 0; i < success->size(); i++) {

		if (success->at(i)->getType() == Cell::CELL_TYPE_WALL) {
			success->erase(success->begin() + i);
			i--;
		}
	
	}
	
	return success;

}