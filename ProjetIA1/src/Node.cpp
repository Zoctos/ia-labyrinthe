#include "../headers/Node.hpp"

Node::Node(Cell *cell, int value) {

	this->fils = new std::vector<Node*>();
	this->content = cell;
	this->value = value;
	this->isChemin = false;

}

void Node::setParent(Node *parent) {
	this->parent = parent;
}

void Node::addFils(Node *fils) {
	this->fils->push_back(fils);
}

std::vector<Node*> *Node::getFils() {
	return this->fils;
}

Node *Node::getParent() {
	return this->parent;
}

Node *Node::getFirstParent() {
	this->isChemin = true;
	if (this->parent == NULL)
		return this;
	else {
		return this->parent->getFirstParent();
	}
}

Cell *Node::getContent() {
	return this->content;
}

int Node::getValue() {
	return this->value;
}

bool Node::isCheminSortie() {
	return this->isChemin;
}

int Node::getProfondeurMax() {
	
	if (fils->size() == 0)
		return 1;
	else {
		
		int max = -1;

		for (int i = 0; i < fils->size(); i++) {
			int m = fils->at(i)->getProfondeurMax();
			if (m > max)
				max = m;
		}

		return max + 1;
	}

}

int Node::getLongueurChemin() {
	if (parent == NULL)
		return 0;
	return 1 + parent->getLongueurChemin();
}

int Node::getNombreNoeuds() {

	if (fils->size() == 0)
		return 0;
	else {

		int value = fils->size();

		for (int i = 0; i < fils->size(); i++) {
			if(fils->at(i)->getFils()->size() != 0)
				value += fils->at(i)->getNombreNoeuds();
		}
		return value;
	}

}

bool Node::isSame(Node *n){
	Cell *c = n->getContent();

	if (c->getX() == content->getX() && c->getY() == content->getY())
		return true;
	else
		return false;
}

void Node::render(sf::RenderWindow *window, int avancementChemin) {

	if (avancementChemin > 0) {
		sf::RectangleShape rect(sf::Vector2f(10.0, 10.0));
		rect.setFillColor(sf::Color::Magenta);
		rect.setPosition(sf::Vector2f(content->getX() * 20 + 5, content->getY() * 20 + 5));
		window->draw(rect);

		if (parent != NULL)
			parent->render(window, avancementChemin-1);
	}

}

void Node::renderFils(sf::RenderWindow *window, int profondeur) {

	if (profondeur > 0) {
		sf::RectangleShape rect(sf::Vector2f(10.0, 10.0));
		rect.setFillColor(sf::Color::Black);
		rect.setPosition(sf::Vector2f(content->getX() * 20 + 5, content->getY() * 20 + 5));
		window->draw(rect);

		for (int i = 0; i < fils->size(); i++) {
			fils->at(i)->renderFils(window, profondeur-1);
		}
	}

}

void Node::renderVersSortie(sf::RenderWindow *window, int profondeur) {

	if (profondeur > 0) {

		sf::RectangleShape rect(sf::Vector2f(10.0, 10.0));
		rect.setFillColor(sf::Color::Magenta);
		rect.setPosition(sf::Vector2f(content->getX() * 20 + 5, content->getY() * 20 + 5));
		window->draw(rect);

		for (int i = 0; i < fils->size(); i++) {
			if (fils->at(i)->isCheminSortie() == true) {
				fils->at(i)->renderVersSortie(window, profondeur - 1);
			}
		}

	}

}