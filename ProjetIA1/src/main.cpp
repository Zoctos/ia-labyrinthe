#include <SFML\Graphics.hpp>

#include "../headers/Labyrinth.hpp"
#include "../headers/EntityIA.hpp"
#include "../headers/EntityIABestFirst.hpp"
#include "../headers/EntityIAUniform.hpp"
#include "../headers/EntityIAAStar.hpp"
#include "../headers/Node.hpp"
#include "../headers/EntityPlayer.h"

#include <thread>
#include <chrono>

int main() {

	sf::RenderWindow *window = new sf::RenderWindow(sf::VideoMode(600, 600), "Projet IA - Semestre 1");

	Labyrinth *labyrinth = new Labyrinth("levels/main.txt");
	
	std::cout << "Lecture du fichier fini" << std::endl;

	EntityIABestFirst *entityFirst = new EntityIABestFirst(labyrinth);
	EntityIAUniform *entityUniform = new EntityIAUniform(labyrinth);
	EntityIAAStar *entityStar = new EntityIAAStar(labyrinth);
	EntityPlayer *entityPlayer = new EntityPlayer(labyrinth);

	sf::Clock clock;

	//Meilleur d'abord
	sf::Time timeBeginFirst = clock.getElapsedTime();
	Node *rootFirst = entityFirst->executeAlgo();
	sf::Time timeEndFirst = clock.getElapsedTime();

	//Cout uniform
	sf::Time timeBeginUniform = clock.getElapsedTime();
	Node *rootUniform = entityUniform->executeAlgo();
	sf::Time timeEndUniform = clock.getElapsedTime();

	//A Star
	sf::Time timeBeginStar = clock.getElapsedTime();
	Node *rootStar = entityStar->executeAlgo();
	sf::Time timeEndStar = clock.getElapsedTime();

	//Node de parcours
	Node *beginFirst = rootFirst->getFirstParent();
	Node *beginUniform = rootFirst->getFirstParent();
	Node *beginStar = rootStar->getFirstParent();

	int profondeurMax = beginStar->getProfondeurMax();
	int profondeur = 0;
	int avancementChemin = 0;
	bool isPlayer = true;

	std::cout << "Temps d'execution : " << std::endl;
	
	std::cout << "Meilleur d'abord : " << timeEndFirst.asMilliseconds() - timeBeginFirst.asMilliseconds() << "ms" << std::endl;
	std::cout << "Cout Uniforme : " << timeEndUniform.asMilliseconds() - timeBeginUniform.asMilliseconds() << "ms" << std::endl;
	std::cout << "A Star : " << timeEndStar.asMilliseconds() - timeBeginStar.asMilliseconds() << "ms" << std::endl;

	std::cout << "--------------------" << std::endl;
	std::cout << "Nombre de noeuds : " << std::endl;
	std::cout << "Meilleur d'abord : " << beginFirst->getNombreNoeuds() << std::endl;
	std::cout << "Cout Uniforme : " << beginUniform->getNombreNoeuds() << std::endl;
	std::cout << "A Star : " << beginStar->getNombreNoeuds() << std::endl;

	std::cout << "--------------------" << std::endl;
	std::cout << "Taille du chemin : " << std::endl;
	std::cout << "Meilleur d'abord : " << rootFirst->getLongueurChemin() << std::endl;
	std::cout << "Cout Uniforme : " << rootUniform->getLongueurChemin() << std::endl;
	std::cout << "A Star : " << rootStar->getLongueurChemin() << std::endl;

	while (window->isOpen()) {

		sf::Event event;
		while (window->pollEvent(event)) {
			if (event.type == sf::Event::Closed)
				window->close();
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left)) {
			entityPlayer->setX(entityPlayer->getX() - 1);
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right)) {
			entityPlayer->setX(entityPlayer->getX() + 1);
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up)) {
			entityPlayer->setY(entityPlayer->getY() - 1);
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down)) {
			entityPlayer->setY(entityPlayer->getY() + 1);
		}

		window->clear();

		labyrinth->render(window);
		
		beginStar->renderFils(window, profondeur);
		entityStar->render(window);

		if (profondeur >= profondeurMax) {
			beginStar->renderVersSortie(window, avancementChemin);
			avancementChemin++;
		}
		else
			profondeur++;

		if (isPlayer)
			entityPlayer->render(window);

		window->display();

		std::this_thread::sleep_for(std::chrono::milliseconds(100));

	}

	return 0;
}